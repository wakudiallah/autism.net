<?php

return array (
  'what_is' => 'Apa itu Autisme ?',
  'desc' => 'Gangguan spektrum autisme (ASD) adalah kecacatan perkembangan yang rumit, biasanya muncul semasa zaman kanak-kanak dan mempengaruhi keupayaan seseorang untuk berkomunikasi dan berinteraksi dengan orang lain.',
  'database_desc' => 'AutismInfo Database adalah pangkalan data yang paling komprehensif dari jenisnya, menyediakan sumber yang boleh dipercayai dan boleh dipercayai untuk maklumat yang berkaitan dengan autisme.',
  'database' => 'AutismInfo Database',
  'service' => 'Perkhidmatan 24 Jam',
  'service_desc' => 'Walaupun penunjuk yang kuat tidak mempunyai kawalan mengenai teks buta, ia adalah hampir tidak tepat.',
);
