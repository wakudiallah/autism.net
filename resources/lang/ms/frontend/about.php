<?php

return array (
  'what_is' => 'What is Autism ?',
  'p1' => 'Autism Spectrum Disorder (ASD) is a developmental disability  that can cause significant social, communication and behavioral challenges. There is often nothing about how people with ASD look that sets them apart from other people, but people with ASD may communicate, interact, behave, and learn in ways that are different from most other people. The learning, thinking, and problem-solving abilities of people with ASD can range from gifted to severely challenged.',
  'p2' => '“A group of complex neurodevelopment disorders characterized by repetitive and characteristic patterns of behavior and difficulties with social communication and interaction. The symptoms are present from early childhood and affect daily functioning.”',
  'p3' => '(National Institute of Neurological Disorders and Stroke)',
);
