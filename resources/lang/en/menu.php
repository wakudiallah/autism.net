<?php

return array (
  'home' => 'Home',
  'about' => 'About Us',
  'faq' => 'FAQ',
  'contact' => 'Contact Us',
  'blog' => 'Blog',
  'autism' => 'What is Autism?',
);
