<?php

return array (
  'what_is' => '什么是自闭症 ？',
  'desc' => '自闭症谱系障碍（ASD）是一种复杂的发育障碍，通常在儿童时期出现，并影响一个人与他人交流和互动的能力。',
  'database_desc' => '自闭症信息数据库是同类中最全面的数据库，可为与自闭症相关的信息提供可靠和可靠的资源。',
  'database' => 'AutismInfo Database',
  'service' => '24小时服务',
  'service_desc' => '即使是功能强大的Pointing也无法控制盲文，这几乎是不合逻辑的。',
);
