<?php

return [
	'title' => 'Inscription',
	'infos' => 'To register please fill this form :',
	'email' => 'Your email',
	'pseudo' => 'Your Nickname',
	'password' => 'Your password',
	'confirm-password' => 'Confirm your password',
	'warning' => 'Attention',
	'warning-name' => '30 characters maximum',
	'warning-password' => 'At least 8 characters',
	'ok' => 'You have been registered !',
	'error'=> 'These credentials do not match our records.',
	'agree' => 'Sebuah. Dengan ini saya mengizinkan Insko.my  untuk menghubungi saya mengenai hal-hal yang menyangkut Insurans saja.'
	
];