<?php

return array (
  'what_is' => 'What is Autism ?',
  'desc' => 'Autism spectrum disorder (ASD) is a complex developmental disability, typically appearing during childhood and affecting a person\'s ability to communicate and interact with others.',
  'database' => 'AutismInfo Database',
  'database_desc' => 'AutismInfo Database  is the most comprehensive database of its kind, providing credible and reliable resources for information related to autism.',
  'service' => '24 Hours Service',
  'service_desc' => 'Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic.',
);
