<?php

return array (
  'what_is' => 'Apa itu Autisme ?',
  'desc' => 'Autism spectrum disorder (ASD) adalah kecacatan perkembangan yang kompleks, biasanya muncul selama masa kanak-kanak dan memengaruhi kemampuan seseorang untuk berkomunikasi dan berinteraksi dengan orang lain.',
  'database_desc' => 'AutismInfo Database adalah database paling komprehensif dari jenisnya, menyediakan sumber daya yang kredibel dan andal untuk informasi yang berkaitan dengan autisme.',
  'database' => 'AutismInfo Database',
  'service' => 'Layanan 24 Jam',
  'service_desc' => 'Bahkan Pointing yang sangat kuat tidak memiliki kontrol tentang teks buta itu hampir tidak ortografis.',
);
