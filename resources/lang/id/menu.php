<?php

return array (
  'home' => 'Beranda',
  'about' => 'Tentang Kami',
  'faq' => 'FAQ',
  'contact' => 'Hubungi Kami',
  'blog' => 'Blog',
  'autism' => 'Apa itu Autisme?',
);
