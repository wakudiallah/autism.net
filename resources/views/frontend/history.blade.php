@extends('layouts.master')
@section('content')


          <style type="text/css">

      
              .tracking-detail {
               padding:3rem 0
              }
              #tracking {
               margin-bottom:1rem
              }
              [class*=tracking-status-] p {
               margin:0;
               font-size:1.1rem;
               color:#fff;
               text-transform:uppercase;
               text-align:center
              }
              [class*=tracking-status-] {
               padding:1.6rem 0
              }
              .tracking-status-intransit {
               background-color:#65aee0
              }
              .tracking-status-outfordelivery {
               background-color:#f5a551
              }
              .tracking-status-deliveryoffice {
               background-color:#f7dc6f
              }
              .tracking-status-delivered {
               background-color:#4cbb87
              }
              .tracking-status-attemptfail {
               background-color:#b789c7
              }
              .tracking-status-error,.tracking-status-exception {
               background-color:#d26759
              }
              .tracking-status-expired {
               background-color:#616e7d
              }
              .tracking-status-pending {
               background-color:#ccc
              }
              .tracking-status-inforeceived {
               background-color:#214977
              }
              .tracking-list {
               border:1px solid #e5e5e5
              }
              .tracking-item {
               border-left:1px solid #e5e5e5;
               position:relative;
               padding:2rem 1.5rem .5rem 2.5rem;
               font-size:.9rem;
               margin-left:3rem;
               min-height:5rem
              }
              .tracking-item:last-child {
               padding-bottom:4rem
              }
              .tracking-item .tracking-date {
               margin-bottom:.5rem
              }
              .tracking-item .tracking-date span {
               color:#888;
               font-size:85%;
               padding-left:.4rem
              }
              .tracking-item .tracking-content {
               padding:.5rem .8rem;
               background-color:#f4f4f4;
               border-radius:.5rem
              }
              .tracking-item .tracking-content span {
               display:block;
               color:#888;
               font-size:85%
              }
              .tracking-item .tracking-icon {
               line-height:2.6rem;
               position:absolute;
               left:-1.3rem;
               width:2.6rem;
               height:2.6rem;
               text-align:center;
               border-radius:50%;
               font-size:1.1rem;
               background-color:#fff;
               color:#fff
              }
              .tracking-item .tracking-icon.status-sponsored {
               background-color:#f68
              }
              .tracking-item .tracking-icon.status-delivered {
               background-color:#4cbb87
              }
              .tracking-item .tracking-icon.status-outfordelivery {
               background-color:#f5a551
              }
              .tracking-item .tracking-icon.status-deliveryoffice {
               background-color:#f7dc6f
              }
              .tracking-item .tracking-icon.status-attemptfail {
               background-color:#b789c7
              }
              .tracking-item .tracking-icon.status-exception {
               background-color:#d26759
              }
              .tracking-item .tracking-icon.status-inforeceived {
               background-color:#214977
              }
              .tracking-item .tracking-icon.status-intransit {
               color:#e5e5e5;
               border:1px solid #e5e5e5;
               font-size:.6rem
              }
              @media(min-width:992px) {
               .tracking-item {
                margin-left:10rem
               }
               .tracking-item .tracking-date {
                position:absolute;
                left:-10rem;
                width:7.5rem;
                text-align:right
               }
               .tracking-item .tracking-date span {
                display:block
               }
               .tracking-item .tracking-content {
                padding:0;
                background-color:transparent
               }
              }
    
    </style>

    <style>
        .modal-backdrop {
        position: fixed;
        top: 0;
        right: 0;
        bottom: 0;
        left: 0;
        z-index: -100 !important;
        background-color: #000;
    }
    </style>


    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->



    <section class="hero-wrap hero-wrap-2" style="background-image: url('http://localhost/autism.info/public/drcare/images/bg_1.jpg');" data-stellar-background-ratio="0.5">
      <div class="overlay"></div>
      <div class="container">
        <div class="row no-gutters slider-text align-items-center justify-content-center">
          <div class="col-md-9 ftco-animate text-center">
            <h1 class="mb-2 bread">About Us</h1>
            <p class="breadcrumbs"><span class="mr-2"><a href="index.html">Home <i class="ion-ios-arrow-forward"></i></a></span> <span>About us <i class="ion-ios-arrow-forward"></i></span></p>
          </div>
        </div>
      </div>
    </section>
		
		<section class="ftco-section ftco-no-pt ftc-no-pb">
			<div class="container">
				<div class="row no-gutters">
					<div class="col-md-3 p-md-5 img img-2 mt-5 mt-md-0" style="background-image: url(http://localhost/autism/public/drcare/images/about.jpg);">
					</div>
					<div class="col-md-9 wrap-about py-4 py-md-5 ftco-animate">
	          <div class="heading-section mb-5">
	          	<div class="pl-md-5 ml-md-5">
		          	<span class="subheading"></span>
		            <h2 class="mb-4" style="font-size: 28px;"> Chronological history of autism</h2>
	            </div>
	          </div>
	          <div class="pl-md-5 ml-md-5 mb-5">
							

              <div class="container">
                 <div class="row">
                    
                    <div class="col-md-12 col-lg-12">
                       <div id="tracking-pre"></div>
                       <div id="tracking">
                          <div class="text-center tracking-status-intransit">
                             <p class="tracking-status text-tight">Chronological history of autism</p>
                          </div>
                          <div class="tracking-list">
                             <div class="tracking-item">
                                <div class="tracking-icon status-intransit">
                                   <svg class="svg-inline--fa fa-circle fa-w-16" aria-hidden="true" data-prefix="fas" data-icon="circle" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg="">
                                      <path fill="currentColor" d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8z"></path>
                                   </svg>
                                   <!-- <i class="fas fa-circle"></i> -->
                                </div>
                                
                                <div class="tracking-content">Eugen Bleuler coined the word "autism" in 1908 among severely withdrawn schizophrenic patients.</div>
                             </div>
                             <div class="tracking-item">
                                <div class="tracking-icon status-intransit">
                                   <svg class="svg-inline--fa fa-circle fa-w-16" aria-hidden="true" data-prefix="fas" data-icon="circle" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg="">
                                      <path fill="currentColor" d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8z"></path>
                                   </svg>
                                   <!-- <i class="fas fa-circle"></i> -->
                                </div>
                                
                                <div class="tracking-content">In 1943 American child psychiatrist Leo Kanner studied 11 children. The children had features of difficulties in social interactions, difficulty in adapting to changes in routines, good memory, sensitivity to stimuli (especially sound), resistance and allergies to food, good intellectual potential, echolalia or propensity to repeat words of the speaker and difficulties in spontaneous activity.</div>
                             </div>
                             <div class="tracking-item">
                                <div class="tracking-icon status-intransit">
                                   <svg class="svg-inline--fa fa-circle fa-w-16" aria-hidden="true" data-prefix="fas" data-icon="circle" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg="">
                                      <path fill="currentColor" d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8z"></path>
                                   </svg>
                                   <!-- <i class="fas fa-circle"></i> -->
                                </div>
                                
                                <div class="tracking-content">In 1944 Hans Asperger, working separately, studied a group of children. His children also resembled Kanner’s descriptions. The children he studied, however, did not have echolalia as a linguistic problem but spoke like grownups. He also mentioned that many of the children were clumsy and different from normal children in terms of fine motor skills.</div>
                             </div>
                             <div class="tracking-item">
                                <div class="tracking-icon status-intransit">
                                   <svg class="svg-inline--fa fa-circle fa-w-16" aria-hidden="true" data-prefix="fas" data-icon="circle" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg="">
                                      <path fill="currentColor" d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8z"></path>
                                   </svg>
                                   <i class="fas fa-circle"></i>
                                </div>
                                
                                <div class="tracking-content">Next Bruno Bettelheim studied the effect of three therapy sessions with children who he called autistic. He claimed that the problem in the children was due to coldness of their mothers. He separated the children from their parents. Kanner and Bettelheim both worked towards making hypothesis that showed autistic children had frigid mothers</div>
                             </div>
                             <div class="tracking-item">
                                <div class="tracking-icon status-intransit">
                                   <svg class="svg-inline--fa fa-circle fa-w-16" aria-hidden="true" data-prefix="fas" data-icon="circle" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg="">
                                      <path fill="currentColor" d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8z"></path>
                                   </svg>
                                   <i class="fas fa-circle"></i>
                                </div>
                                
                                <div class="tracking-content">Bernard Rimland was a psychologist and parent of a child with autism. He disagreed with Bettelheim. He did not agree that the cause of his son’s autism was due to either his or his wife’s parenting skills. In 1964, Bernard Rimland published, Infantile Autism: The Syndrome and its Implications for a Neural Theory of Behavior.</div>
                             </div>
                             <div class="tracking-item">
                                <div class="tracking-icon status-intransit">
                                   <svg class="svg-inline--fa fa-circle fa-w-16" aria-hidden="true" data-prefix="fas" data-icon="circle" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg="">
                                      <path fill="currentColor" d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8z"></path>
                                   </svg>
                                   <i class="fas fa-circle"></i>
                                </div>
                               
                                <div class="tracking-content">Autism came to be better known in the 1970’s. The Erica Foundation started education and therapy for psychotic children in the beginning of the 80s. Many parents still confused autism with mental retardation and psychosis.</div>
                             </div>
                             <div class="tracking-item">
                                <div class="tracking-icon status-intransit">
                                   <svg class="svg-inline--fa fa-circle fa-w-16" aria-hidden="true" data-prefix="fas" data-icon="circle" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg="">
                                      <path fill="currentColor" d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8z"></path>
                                   </svg>
                                   <i class="fas fa-circle"></i>
                                </div>
                               
                                <div class="tracking-content">It was in 1980’s that Asperger’s work was translated to English and published and came into knowledge.</div>
                             </div>
                             <div class="tracking-item">
                                <div class="tracking-icon status-intransit">
                                   <svg class="svg-inline--fa fa-circle fa-w-16" aria-hidden="true" data-prefix="fas" data-icon="circle" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg="">
                                      <path fill="currentColor" d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8z"></path>
                                   </svg>
                                   <i class="fas fa-circle"></i>
                                </div>
                                
                                <div class="tracking-content">It was in the 1980’s that research on autism gained momentum. It was increasingly believed that parenting had no role in causation of autism and there were neurological disturbances and other genetic ailments like tuberous sclerosis, metabolic disturbances like PKU or chromosomal abnormalities like fragile X syndrome.</div>
                             </div>
                             <div class="tracking-item">
                                <div class="tracking-icon status-intransit">
                                   <svg class="svg-inline--fa fa-circle fa-w-16" aria-hidden="true" data-prefix="fas" data-icon="circle" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg="">
                                      <path fill="currentColor" d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8z"></path>
                                   </svg>
                                   <i class="fas fa-circle"></i>
                                </div>
                                
                                <div class="tracking-content">Lorna Wing, along with Christopher Gillberg at BNK (Children's Neuro-Psychiatric Clinic) in Sweden in the 1980’s found the Wing’s triad of disturbed mutual contact, disturbed mutual communication and limited imagination. In the 1990’s they added another factor making it a square. The factor was limited planning ability.</div>
                             </div>
                             <div class="tracking-item">
                                <div class="tracking-icon status-intransit">
                                   <svg class="svg-inline--fa fa-circle fa-w-16" aria-hidden="true" data-prefix="fas" data-icon="circle" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg="">
                                      <path fill="currentColor" d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8z"></path>
                                   </svg>
                                   <!-- <i class="fas fa-circle"></i> -->
                                </div>
                                
                                <div class="tracking-content">Ole Ivar Lovaas studied and furthered behavioural analysis and treatment of children with autism. Lovaas achieved limited success at first with his experimental behaviour analysis. He developed it to target younger children (less than 5 years of age) and implemented treatment at home and increased the intensity (a measurement of the amount of “therapy time”) to about 40 hours weekly. Lovaas wrote Teaching Developmentally Disabled Children: The Me Book in 1981. In 2002, Lovaas wrote, Teaching Individuals With Developmental Delays: Basic Intervention Techniques</div>
                             </div>
                             
                          </div>
                       </div>
                    </div>
                 </div>
              </div>


						

						
            </div>
					</div>
				</div>
			</div>
		</section>
		
		<section class="ftco-intro" style="background-image: url(images/bg_3.jpg);" data-stellar-background-ratio="0.5">
			<div class="overlay"></div>
			<div class="container">
				<div class="row">
					<div class="col-md-9">
						<h2>We Provide Free Health Care Consultation</h2>
						<p class="mb-0">Your Health is Our Top Priority with Comprehensive, Affordable medical.</p>
						<p></p>
					</div>
					<div class="col-md-3 d-flex align-items-center">
						<p class="mb-0"><a href="#" class="btn btn-secondary px-4 py-3">Free Consutation</a></p>
					</div>
				</div>
			</div>
		</section>
		
		
    <section class="ftco-section testimony-section bg-light">
      <div class="container">
        <div class="row justify-content-center mb-5 pb-2">
          <div class="col-md-8 text-center heading-section ftco-animate">
          	<span class="subheading">Testimonials</span>
            <h2 class="mb-4">Our Patients Says About Us</h2>
            <p>Separated they live in. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country</p>
          </div>
        </div>
        <div class="row ftco-animate justify-content-center">
          <div class="col-md-8">
            <div class="carousel-testimony owl-carousel">
              <div class="item">
                <div class="testimony-wrap d-flex">
                  <div class="user-img mr-4" style="background-image: url(images/person_1.jpg)">
                  </div>
                  <div class="text ml-2 bg-light">
                  	<span class="quote d-flex align-items-center justify-content-center">
                      <i class="icon-quote-left"></i>
                    </span>
                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                    <p class="name">Racky Henderson</p>
                    <span class="position">Farmer</span>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="testimony-wrap d-flex">
                  <div class="user-img mr-4" style="background-image: url(images/person_2.jpg)">
                  </div>
                  <div class="text ml-2 bg-light">
                  	<span class="quote d-flex align-items-center justify-content-center">
                      <i class="icon-quote-left"></i>
                    </span>
                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                    <p class="name">Henry Dee</p>
                    <span class="position">Businessman</span>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="testimony-wrap d-flex">
                  <div class="user-img mr-4" style="background-image: url(images/person_3.jpg)">
                  </div>
                  <div class="text ml-2 bg-light">
                  	<span class="quote d-flex align-items-center justify-content-center">
                      <i class="icon-quote-left"></i>
                    </span>
                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                    <p class="name">Mark Huff</p>
                    <span class="position">Students</span>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="testimony-wrap d-flex">
                  <div class="user-img mr-4" style="background-image: url(images/person_4.jpg)">
                  </div>
                  <div class="text ml-2 bg-light">
                  	<span class="quote d-flex align-items-center justify-content-center">
                      <i class="icon-quote-left"></i>
                    </span>
                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                    <p class="name">Rodel Golez</p>
                    <span class="position">Striper</span>
                  </div>
                </div>
              </div>
              <div class="item">
                <div class="testimony-wrap d-flex">
                  <div class="user-img mr-4" style="background-image: url(images/person_1.jpg)">
                  </div>
                  <div class="text ml-2 bg-light">
                  	<span class="quote d-flex align-items-center justify-content-center">
                      <i class="icon-quote-left"></i>
                    </span>
                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
                    <p class="name">Ken Bosh</p>
                    <span class="position">Manager</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>


		
    
@stop