@section('header')
<!DOCTYPE html>
<html lang="en">
  <head>
    <title>Autism.info</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    <link href="https://fonts.googleapis.com/css?family=Work+Sans:100,200,300,400,500,600,700,800,900" rel="stylesheet">

    <link rel="stylesheet" href="{{url('/drcare/css/open-iconic-bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{url('/drcare/css/animate.css')}}">
    
    <link rel="stylesheet" href="{{url('/drcare/css/owl.carousel.min.css')}}">
    <link rel="stylesheet" href="{{url('/drcare/css/owl.theme.default.min.css')}}">
    <link rel="stylesheet" href="{{url('/drcare/css/magnific-popup.css')}}">

    <link rel="stylesheet" href="{{url('/drcare/css/aos.css')}}">

    <link rel="stylesheet" href="{{url('/drcare/css/ionicons.min.css')}}">

    <link rel="stylesheet" href="{{url('/drcare/css/bootstrap-datepicker.css')}}">
    <link rel="stylesheet" href="{{url('/drcare/css/jquery.timepicker.css')}}">

    
    <link rel="stylesheet" href="{{url('/drcare/css/flaticon.css')}}">
    <link rel="stylesheet" href="{{url('/drcare/css/icomoon.css')}}">
    <link rel="stylesheet" href="{{url('/drcare/css/style.css')}}">
  </head>
  <body>
    <nav class="navbar py-4 navbar-expand-lg ftco_navbar navbar-light bg-light flex-row">
    	<div class="container">
    		<div class="row no-gutters d-flex align-items-start align-items-center px-3 px-md-0">
    			<div class="col-lg-2 pr-4 align-items-center">
		    		<a class="navbar-brand" href="index.html">Autism.<span>info</span></a>
	    		</div>
	    		<div class="col-lg-10 d-none d-md-block">
		    		<div class="row d-flex">
			    		<div class="col-md-4 pr-4 d-flex topper align-items-center">
			    			<div class="icon bg-white mr-2 d-flex justify-content-center align-items-center"><span class="icon-map"></span></div>
						    <span class="text">Address: Kuala Lumpur, Malaysia</span>
					    </div>
					    <div class="col-md pr-4 d-flex topper align-items-center">
					    	<div class="icon bg-white mr-2 d-flex justify-content-center align-items-center"><span class="icon-paper-plane"></span></div>
						    <span class="text">Email: autism.info@gmail.com</span>
					    </div>
					    <div class="col-md pr-4 d-flex topper align-items-center">
					    	<div class="icon bg-white mr-2 d-flex justify-content-center align-items-center"><span class="icon-phone2"></span></div>
						    <span class="text">Phone: + 603 10290192 </span>
					    </div>
				    </div>
			    </div>
		    </div>
		  </div>
    </nav>
	  <nav class="navbar navbar-expand-lg navbar-dark bg-dark ftco-navbar-light" id="ftco-navbar">
	    <div class="container d-flex align-items-center">
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
	        <span class="oi oi-menu"></span> {{trans('menu.menu')}}
	      </button>
	      <p class="button-custom order-lg-last mb-0"><a href="appointment.html" class="btn btn-secondary py-2 px-3">Make An Appointment</a></p>
	      <div class="collapse navbar-collapse" id="ftco-nav">
	        <ul class="navbar-nav mr-auto">
	        	<li class="nav-item active"><a href="index.html" class="nav-link pl-0">{{trans('menu.home')}}</a></li>
	        	<li class="nav-item"><a href="{{url('/what-is-autism')}}" class="nav-link">Type</a></li>
	        	<li class="nav-item"><a href="{{url('/history-of-autism')}}" class="nav-link">Early Detection</a></li>

	        	<li class="nav-item">
					<div class="b-topBar__lang" style="margin-top: 14px !important">
						<div class="dropdown">
							<!--<a href="#" class="dropdown-toggle" data-toggle='dropdown'><b>{{trans('menu/header.language')}}</b></a>-->
						    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
						      <img src="" width="20px" height="13px"><span class="fa fa-caret-down"></span>&nbsp; Screening
						    </a>
							    <ul class="dropdown-menu">
					                <li>
					                    <a href="">&nbsp; Screening Insturat</a>
					                    <a href="">&nbsp; Apps </a>
					                </li>
							    </ul>
						</div>
					</div>
				</li>
				<li class="nav-item"><a href="pricing.html" class="nav-link">Treatment</a></li>

				<li class="nav-item">
					<div class="b-topBar__lang" style="margin-top: 14px !important">
						<div class="dropdown">
							<!--<a href="#" class="dropdown-toggle" data-toggle='dropdown'><b>{{trans('menu/header.language')}}</b></a>-->
						    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
						      <img src="" width="20px" height="13px"><span class="fa fa-caret-down"></span>&nbsp; Therapy
						    </a>
							    <ul class="dropdown-menu">
					                
					                <li>
					                    <a href="">&nbsp; Sensory Therapy</a>
					                </li>
					                <li>
					                    <a href="">&nbsp; Hippo </a>
					                </li>
					                <li>
					                    <a href="">&nbsp; Lego </a>
					                </li>
					                <li>
					                    <a href="">&nbsp; Speech </a>
					                </li>
					                <li>
					                    <a href="">&nbsp; Cognitive </a>
					                </li>
					                <li>
					                    <a href="">&nbsp; Edigames </a>
					                </li>
							    </ul>
						</div>
					</div>
				</li>


	        	<li class="nav-item"><a href="blog.html" class="nav-link">{{trans('menu.blog')}}</a></li>
	          	<li class="nav-item"><a href="contact.html" class="nav-link">{{trans('menu.contact')}}</a></li>
	          	<li class="nav-item">
					<div class="b-topBar__lang" style="margin-top: 14px !important">
						<div class="dropdown">
							<!--<a href="#" class="dropdown-toggle" data-toggle='dropdown'><b>{{trans('menu/header.language')}}</b></a>-->
						    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
						      <img src="{!! asset('img/flags/1x1/' .  Config::get('languages')[App::getLocale()]  . '.svg') !!}" width="20px" height="13px"><span class="fa fa-caret-down"></span>&nbsp; {{Config::get('languages')[App::getLocale()]}}
						    </a>
							    <ul class="dropdown-menu">
							        @foreach(Config::get('languages') as $lang => $language)
							            @if($lang != App::getLocale())
							                <li>
							                    <a href="{{ route('lang.switch', $lang) }}"><img src="{!! asset('img/flags/1x1/' . $language . '.svg') !!}" width="20px" height="13px">&nbsp; {{$language}}</a>

							                </li>
							            @endif
							        @endforeach
							    </ul>
						</div>
					</div>
				</li>
	        </ul>
	      </div>
	    </div>
	  </nav>
    <!-- END nav -->
@stop

		
		
		